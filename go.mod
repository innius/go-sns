module bitbucket.org/innius/go-sns

require (
	github.com/aws/aws-lambda-go v1.8.1
	github.com/aws/aws-sdk-go v1.22.2
	github.com/gopherjs/gopherjs v0.0.0-20190328170749-bb2674552d8f // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.2.8
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.4.0
	github.com/smartystreets/assertions v0.0.0-20190116191733-b6c0e53d7304 // indirect
	github.com/smartystreets/goconvey v0.0.0-20181108003508-044398e4856c
	github.com/stretchr/testify v1.2.2
	github.com/valyala/fasttemplate v1.0.1 // indirect
	golang.org/x/net v0.0.0-20190327214358-63eda1eb0650
)

go 1.13
