package events

import (
	"bitbucket.org/innius/go-sns/types"
	"context"
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sns/snsiface"
	"github.com/pkg/errors"
)

type eventMessage struct {
	Source  string `json:"source"`
	Event   string `json:"event"`
	Message interface{}
}

func NewPublisher(client snsiface.SNSAPI, topicArn string) *publisher {
	return &publisher{
		client:   client,
		topicArn: topicArn,
	}
}

type Publisher interface {
	// publish an event
	Publish(context.Context, interface{}) error
}

type publisher struct {
	client   snsiface.SNSAPI
	topicArn string
}

func (p publisher) Publish(c context.Context, event interface{}) error {
	msg := eventMessage{
		Event:   types.EventType(event),
		Source:  types.EventSource(event),
		Message: event,
	}
	return p.publish(c, msg)
}

const (
	sourceKey = "source"
	eventKey  = "event"
)

func (p publisher) publish(c context.Context, event eventMessage) error {
	bits, err := json.Marshal(event.Message)
	if err != nil {
		return errors.Wrap(err, "Unable to marshal event")
	}

	_, err = p.client.PublishWithContext(c, &sns.PublishInput{
		Message: aws.String(string(bits)),
		MessageAttributes: map[string]*sns.MessageAttributeValue{
			sourceKey: {DataType: aws.String("String"), StringValue: aws.String(event.Source)},
			eventKey:  {DataType: aws.String("String"), StringValue: aws.String(event.Event)},
		},
		TopicArn: aws.String(p.topicArn),
	})
	if err != nil {
		return errors.Wrap(err, "Failed to publish event")
	}
	return nil
}
