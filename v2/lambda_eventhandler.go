package events

import (
	"bitbucket.org/innius/go-sns/subscriber"
	"bitbucket.org/innius/go-sns/types"
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/labstack/gommon/log"
)

// NewLambdaEventHandler instantiates a new lambda sns handler
// it accepts a slice of event handlers
func NewLambdaEventHandler(handlers ... subscriber.EventHandler) *LambdaEventHandler {
	h := map[string]subscriber.EventHandler{}

	for _, handler := range handlers {
		h[types.EventSource(handler)] = handler
	}

	return &LambdaEventHandler{
		handlers: h,
	}
}

func SetDebug() {
	log.SetLevel(log.DEBUG)
}

// LambdaEventHandler provides an domain event handler for lambda sns events
type LambdaEventHandler struct {
	handlers map[string]subscriber.EventHandler
}

func (s *LambdaEventHandler) HandleEvent(c context.Context, evt events.SNSEntity) error {
	event := types.RawEvent{
		RawMessage: []byte(evt.Message),
		EventType:  eventtype(evt),
	}
	log.Debugf("received event %+v", evt)
	log.Debugf("find handler for event source %s", eventsource(evt))
	if h, ok := s.handlers[eventsource(evt)]; ok {
		return h.EventHandler(c, event)
	}
	log.Warnf("no event handler found for event source %s", eventsource(evt))
	return nil
}

func eventtype(evt events.SNSEntity) string {
	return getMessageAttributeValue(eventKey, evt.MessageAttributes)
}

func eventsource(evt events.SNSEntity) string {
	return getMessageAttributeValue(sourceKey, evt.MessageAttributes)
}

func getMessageAttributeValue(key string, attrs map[string]interface{}) string {
	if v, ok := attrs[key]; ok {
		vv := v.(map[string]interface{})
		return vv["Value"].(string)
	}
	return ""
}

// convenience func for event subscription testing
// use this to test a subscription implementation
func NewTestEvent(event interface{}) events.SNSEntity {
	bits, err := json.Marshal(event)
	if err != nil {
		panic(err)
	}
	return events.SNSEntity{
		Message: string(bits),
		MessageAttributes: map[string]interface{}{
			sourceKey: map[string]interface{}{
				"Value": types.EventSource(event),
			},
			eventKey: map[string]interface{}{
				"Value": types.EventType(event),
			},
		},
	}
}
