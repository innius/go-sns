package events

import (
	"context"
	"testing"

	sensorevents "bitbucket.org/innius/go-sns/events/sensors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sns/snsiface"
	. "github.com/smartystreets/goconvey/convey"
)

type mockSNS struct {
	snsiface.SNSAPI
	*sns.PublishInput
}

func (m *mockSNS) PublishWithContext(c aws.Context, msg *sns.PublishInput, opts ...request.Option) (*sns.PublishOutput, error) {
	m.PublishInput = msg
	return nil, nil
}

func (m *mockSNS) entity() events.SNSEntity {
	attrs := map[string]interface{}{}

	for key, attr := range m.MessageAttributes {
		a := map[string]interface{}{}
		a["Value"] = aws.StringValue(attr.StringValue)
		attrs[key] = a
	}
	return events.SNSEntity{
		MessageAttributes: attrs,
		Message:           aws.StringValue(m.Message),
	}
}

type mySubscription struct {
	subscription *sensorevents.SensorEventSubscription
	executed     bool
}

func TestSubscription_HandleEvent(t *testing.T) {
	Convey("Subscription test", t, func() {
		event := sensorevents.SensorCreatedEvent{
			SensorID: "foo",
		}
		Convey("publish a message", func() {
			mock := &mockSNS{}
			pub := NewPublisher(mock, "")
			So(pub.Publish(context.Background(), event), ShouldBeNil)

		})
		Convey("test the subscription", func() {
			handler := &mySubscription{
				subscription: &sensorevents.SensorEventSubscription{},
			}

			handler.subscription.OnSensorCreated(func(c context.Context, e sensorevents.SensorCreatedEvent) error {
				So(e.SensorID, ShouldEqual, event.SensorID)
				handler.executed = true
				return nil
			})
			subscription := NewLambdaEventHandler(handler.subscription)

			So(subscription.HandleEvent(context.Background(), NewTestEvent(event)), ShouldBeNil)
			So(handler.executed, ShouldBeTrue)
		})
	})
}
