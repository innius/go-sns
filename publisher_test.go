package publisher

import (
	"testing"

	"bitbucket.org/innius/go-sns/events/machine"
	"bitbucket.org/innius/go-sns/subscriber"
	"bitbucket.org/innius/go-sns/types"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"golang.org/x/net/context"
)

func TestIntegrationTest(t *testing.T) {
	sess := session.New()

	snsClient := sns.New(sess, sess.Config.WithEndpoint("http://localhost:9911").WithRegion("us-east-1"))
	res, err := snsClient.CreateTopic(&sns.CreateTopicInput{Name: aws.String("foobarzz")})
	assert.Nil(t, err)
	topicArn := aws.StringValue(res.TopicArn)

	e := echo.New()

	snsSubscr := subscriber.NewSubscriber(snsClient, topicArn, "http://localhost:1323/sns/machineevents")
	mySubscription := &machineevents.MachineEventSubscription{}

	e.POST("/sns/machineevents", snsSubscr.Handler(mySubscription))
	go func() {
		e.Logger.Fatal(e.Start(":1323"))
	}()

	snsSubscr.Subscribe()

	pub := SnsPublisher{topicArn: topicArn, client: snsClient}

	evt := machineevents.CompanyCreatedEvent{Name: "foo"}
	raw, _ := types.NewEventMessage(evt)

	pub.PublishRawEvent(context.Background(), raw)

	var receivedEvent machineevents.CompanyCreatedEvent
	done := make(chan struct{})
	go func() {
		mySubscription.OnCompanyCreated(func(c context.Context, e machineevents.CompanyCreatedEvent) error {
			receivedEvent = e
			done <- struct{}{}
			return err
		})
	}()

	<-done

	assert.Equal(t, evt, receivedEvent)
}

func TestSnsPublisher_PublishRawEvent(t *testing.T) {
	p := SnsPublisher{topicArn: "", client: &SNSMock{}}
	event, _ := types.NewEventMessage(machineevents.CompanyCreatedEvent{})

	err := p.PublishRawEvent(context.TODO(), event)
	assert.NoError(t, err)
}

type SNSMock struct {
	*sns.SNS
}

func (m *SNSMock) PublishWithContext(ctx aws.Context, input *sns.PublishInput, options ...request.Option) (*sns.PublishOutput, error) {
	return nil, nil
}
