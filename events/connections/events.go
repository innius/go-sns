package connections

// MachineConnectedEvent is sent when a machine is connected
type MachineConnectedEvent struct {
	MachineID string `json:"machinetrn"`
	Timestamp int64  `json:"timestamp"`
}

// MachineConnectedEvent is sent when a machine is disconnected
type MachineDisconnectedEvent struct {
	MachineID     string `json:"machinetrn"`
	LastHeartBeat int64  `json:"last_heart_beat"`
	Timestamp     int64  `json:"timestamp"`
}
