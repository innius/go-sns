package connections

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-sns/types"
	"github.com/sirupsen/logrus"
)

type MachineEventSubscription struct {
	onMachineConnectedHandler    func(context.Context, MachineConnectedEvent) error
	onMachineDisconnectedHandler func(context.Context, MachineDisconnectedEvent) error
}

func (m *MachineEventSubscription) EventHandler(c context.Context, e types.RawEvent) error {
	switch e.EventType {
	case types.EventType(MachineConnectedEvent{}):
		return m.onMachineConnected(c, e)
	case types.EventType(MachineDisconnectedEvent{}):
		return m.onMachineDisconnected(c, e)
	}
	logrus.Errorf("unsupported event type %v", e.EventType)
	return nil
}

func (m *MachineEventSubscription) OnMachineConnected(h func(c context.Context, h MachineConnectedEvent) error) {
	m.onMachineConnectedHandler = h
}

func (m *MachineEventSubscription) onMachineConnected(c context.Context, e types.RawEvent) error {
	if m.onMachineConnectedHandler == nil {
		return nil
	}
	event := MachineConnectedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineConnectedHandler(c, event)
}

func (m *MachineEventSubscription) OnMachineDisconnected(h func(c context.Context, h MachineDisconnectedEvent) error) {
	m.onMachineDisconnectedHandler = h
}

func (m *MachineEventSubscription) onMachineDisconnected(c context.Context, e types.RawEvent) error {
	if m.onMachineConnectedHandler == nil {
		return nil
	}
	event := MachineDisconnectedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineDisconnectedHandler(c, event)
}
