package sensorevents

//TODO decide whether batchSensorConfigs need their own events for sensorCreation

// SensorCreatedEvent is sent when a new sensor is configured
type SensorCreatedEvent struct {
	CompanyID      string         `json:"company_id"`
	MachineID      string         `json:"machine_id"`
	SensorID       string         `json:"sensor_id"`
	BatchSensor    string         `json:"batch_sensor"`
	SensorKind     SensorKind     `json:"sensor_kind"`
	SensorDataType SensorDataType `json:"sensor_data_type"`
	Rate           int            `json:"rate"`
}

// SensorDeletedEvent is sent when a sensor is deleted
type SensorDeletedEvent struct {
	CompanyID string `json:"company_id"`
	MachineID string `json:"machine_id"`
	SensorID  string `json:"sensor_id"`
	Rate      int    `json:"rate"`
}

type SensorKindChangedEvent struct {
	CompanyID string     `json:"company_id"`
	MachineID string     `json:"machine_id"`
	SensorID  string     `json:"sensor_id"`
	NewKind   SensorKind `json:"new_kind"`
}

type SensorRateChangedEvent struct {
	CompanyID string `json:"company_id"`
	MachineID string `json:"machine_id"`
	SensorID  string `json:"sensor_id"`
	NewValue  int    `json:"new_value"`
}

type SensorBatchChangedEvent struct {
	CompanyID      string         `json:"company_id"`
	MachineID      string         `json:"machine_id"`
	SensorID       string         `json:"sensor_id"`
	SensorDataType SensorDataType `json:"sensor_data_type"`
	NewBatchSensor string         `json:"new_batch_sensor"`
}

type SensorDataTypeChangedEvent struct {
	CompanyID string         `json:"company_id"`
	MachineID string         `json:"machine_id"`
	SensorID  string         `json:"sensor_id"`
	NewType   SensorDataType `json:"new_type"`
}

type SensorKind int

type SensorDataType string

const (
	Physical SensorKind = iota
	Virtual
	Manual
	Scheduled

	Discrete   SensorDataType = "discrete"
	Continuous SensorDataType = "continuous"
	Counter    SensorDataType = "counter"
)
