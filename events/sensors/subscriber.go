package sensorevents

import (
	"bitbucket.org/innius/go-sns/types"
	"context"
	"encoding/json"
)

type SensorEventSubscription struct {
	onSensorCreatedHandler     func(context.Context, SensorCreatedEvent) error
	onSensorDeletedHandler     func(context.Context, SensorDeletedEvent) error
	onSensorRateChangedHandler func(context.Context, SensorRateChangedEvent) error
	onSensorKindChangedHandler func(ctx context.Context, event SensorKindChangedEvent) error
	onSensorBatchChangedHandler func(ctx context.Context, event SensorBatchChangedEvent) error
	onSensorDataTypeChangedHandler func(ctx context.Context, event SensorDataTypeChangedEvent) error
}

func (m *SensorEventSubscription) EventHandler(c context.Context, e types.RawEvent) error {
	switch e.EventType {
	case types.EventType(SensorCreatedEvent{}):
		return m.onSensorCreated(c, e)
	case types.EventType(SensorDeletedEvent{}):
		return m.onSensorDeleted(c, e)
	case types.EventType(SensorKindChangedEvent{}):
		return m.onSensorKindChanged(c, e)
	case types.EventType(SensorRateChangedEvent{}):
		return m.onSensorRateChanged(c, e)
	case types.EventType(SensorBatchChangedEvent{}):
		return m.onSensorBatchChanged(c, e)
	case types.EventType(SensorDataTypeChangedEvent{}):
		return m.onSensorDataTypeChanged(c, e)
	}

	return nil
}

func (m *SensorEventSubscription) OnSensorCreated(h func(context.Context, SensorCreatedEvent) error) {
	m.onSensorCreatedHandler = h
}

func (m *SensorEventSubscription) onSensorCreated(c context.Context, e types.RawEvent) error {
	if m.onSensorCreatedHandler == nil {
		return nil
	}
	event := SensorCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorCreatedHandler(c, event)
}

func (m *SensorEventSubscription) OnSensorDeleted(h func(context.Context, SensorDeletedEvent) error) {
	m.onSensorDeletedHandler = h
}

func (m *SensorEventSubscription) onSensorDeleted(c context.Context, e types.RawEvent) error {
	if m.onSensorDeletedHandler == nil {
		return nil
	}
	event := SensorDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorDeletedHandler(c, event)
}

func (m *SensorEventSubscription) OnSensorKindChanged(h func(ctx context.Context, event SensorKindChangedEvent) error) {
	m.onSensorKindChangedHandler = h
}

func (m *SensorEventSubscription) onSensorKindChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorKindChangedHandler == nil {
		return nil
	}
	event := SensorKindChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorKindChangedHandler(c, event)
}

func (m *SensorEventSubscription) OnSensorRateChanged(h func(context.Context, SensorRateChangedEvent) error) {
	m.onSensorRateChangedHandler = h
}

func (m *SensorEventSubscription) onSensorRateChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorRateChangedHandler == nil {
		return nil
	}
	event := SensorRateChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorRateChangedHandler(c, event)
}

func (m *SensorEventSubscription) OnSensorBatchChanged(h func(context.Context, SensorBatchChangedEvent) error) {
	m.onSensorBatchChangedHandler = h
}

func (m *SensorEventSubscription) onSensorBatchChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorBatchChangedHandler == nil {
		return nil
	}
	event := SensorBatchChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorBatchChangedHandler(c, event)
}

func (m *SensorEventSubscription) OnSensorDataTypeChanged(h func(context.Context, SensorDataTypeChangedEvent) error) {
	m.onSensorDataTypeChangedHandler = h
}

func (m *SensorEventSubscription) onSensorDataTypeChanged(c context.Context, e types.RawEvent) error {
	if m.onSensorDataTypeChangedHandler == nil {
		return nil
	}
	event := SensorDataTypeChangedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onSensorDataTypeChangedHandler(c, event)
}