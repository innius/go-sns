package machineevents

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-sns/types"
	"github.com/sirupsen/logrus"
)

type MachineEventSubscription struct {
	onCompanyCreatedHandler   func(context.Context, CompanyCreatedEvent) error
	onCompanyDeletedHandler   func(context.Context, CompanyDeletedEvent) error
	onPersonCreatedHandler    func(context.Context, PersonCreatedEvent) error
	onPersonDeletedHandler    func(context.Context, PersonDeletedEvent) error
	onAdminCreatedHandler     func(context.Context, AdminCreatedEvent) error
	onAdminDeletedHandler     func(context.Context, AdminDeletedEvent) error
	onRelationVerifiedHandler func(context.Context, RelationVerifiedEvent) error
	onRelationDeletedHandler  func(context.Context, RelationDeletedEvent) error
	onMachineSharedHandler    func(context.Context, MachineSharedEvent) error
	onMachineUnsharedHandler  func(context.Context, MachineUnsharedEvent) error
	onMachineCreatedHandler   func(context.Context, MachineCreatedEvent) error
	onMachineDeletedHandler   func(context.Context, MachineDeletedEvent) error
}

func (m *MachineEventSubscription) EventHandler(c context.Context, e types.RawEvent) error {
	switch e.EventType {
	case types.EventType(CompanyCreatedEvent{}):
		return m.onCompanyCreated(c, e)
	case types.EventType(CompanyDeletedEvent{}):
		return m.onCompanyDeleted(c, e)
	case types.EventType(PersonCreatedEvent{}):
		return m.onPersonCreated(c, e)
	case types.EventType(PersonDeletedEvent{}):
		return m.onPersonDeleted(c, e)
	case types.EventType(AdminCreatedEvent{}):
		return m.onAdminCreated(c, e)
	case types.EventType(AdminDeletedEvent{}):
		return m.onAdminDeleted(c, e)
	case types.EventType(RelationVerifiedEvent{}):
		return m.onRelationVerified(c, e)
	case types.EventType(RelationDeletedEvent{}):
		return m.onRelationDeleted(c, e)
	case types.EventType(MachineSharedEvent{}):
		return m.onMachineShared(c, e)
	case types.EventType(MachineUnsharedEvent{}):
		return m.onMachineUnshared(c, e)
	case types.EventType(MachineCreatedEvent{}):
		return m.onMachineCreated(c, e)
	case types.EventType(MachineDeletedEvent{}):
		return m.onMachineDeleted(c, e)
	}
	logrus.Errorf("unsupported event type %v", e.EventType)
	return nil
}

// OnCompanyCreated registers a handler for CompanyCreatedEvent
func (m *MachineEventSubscription) OnCompanyCreated(h func(c context.Context, h CompanyCreatedEvent) error) {
	m.onCompanyCreatedHandler = h
}

func (m *MachineEventSubscription) onCompanyCreated(c context.Context, e types.RawEvent) error {
	if m.onCompanyCreatedHandler == nil {
		return nil
	}
	event := CompanyCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onCompanyCreatedHandler(c, event)
}

// OnCompanyDeleted registers a handler for CompanyDeletedEvent
func (m *MachineEventSubscription) OnCompanyDeleted(h func(c context.Context, h CompanyDeletedEvent) error) {
	m.onCompanyDeletedHandler = h
}

func (m *MachineEventSubscription) onCompanyDeleted(c context.Context, e types.RawEvent) error {
	if m.onCompanyDeletedHandler == nil {
		return nil
	}
	event := CompanyDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onCompanyDeletedHandler(c, event)
}

// OnPersonDeleted registers a handler for PersonDeletedEvent
func (m *MachineEventSubscription) OnPersonDeleted(h func(c context.Context, h PersonDeletedEvent) error) {
	m.onPersonDeletedHandler = h
}

func (m *MachineEventSubscription) onPersonDeleted(c context.Context, e types.RawEvent) error {
	if m.onPersonDeletedHandler == nil {
		return nil
	}
	event := PersonDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onPersonDeletedHandler(c, event)
}

// OnPersonCreated registers a handler for PersonCreatedEvent
func (m *MachineEventSubscription) OnPersonCreated(h func(c context.Context, h PersonCreatedEvent) error) {
	m.onPersonCreatedHandler = h
}

func (m *MachineEventSubscription) onPersonCreated(c context.Context, e types.RawEvent) error {
	if m.onPersonCreatedHandler == nil {
		return nil
	}
	event := PersonCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onPersonCreatedHandler(c, event)
}

// OnAdminDeleted registers a handler for AdminDeletedEvent
func (m *MachineEventSubscription) OnAdminDeleted(h func(c context.Context, h AdminDeletedEvent) error) {
	m.onAdminDeletedHandler = h
}

func (m *MachineEventSubscription) onAdminDeleted(c context.Context, e types.RawEvent) error {
	if m.onAdminDeletedHandler == nil {
		return nil
	}
	event := AdminDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onAdminDeletedHandler(c, event)
}

// OnAdminCreated registers a handler for AdminCreatedEvent
func (m *MachineEventSubscription) OnAdminCreated(h func(c context.Context, h AdminCreatedEvent) error) {
	m.onAdminCreatedHandler = h
}

func (m *MachineEventSubscription) onAdminCreated(c context.Context, e types.RawEvent) error {
	if m.onAdminCreatedHandler == nil {
		return nil
	}
	event := AdminCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onAdminCreatedHandler(c, event)
}

// OnRelationVerified registers a handler for RelationVerifiedEventEvent
func (m *MachineEventSubscription) OnRelationVerified(h func(c context.Context, h RelationVerifiedEvent) error) {
	m.onRelationVerifiedHandler = h
}

func (m *MachineEventSubscription) onRelationVerified(c context.Context, e types.RawEvent) error {
	if m.onRelationVerifiedHandler == nil {
		return nil
	}
	event := RelationVerifiedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onRelationVerifiedHandler(c, event)
}

// OnRelationDeleted registers a handler for RelationDeletedEvent
func (m *MachineEventSubscription) OnRelationDeleted(h func(c context.Context, h RelationDeletedEvent) error) {
	m.onRelationDeletedHandler = h
}

func (m *MachineEventSubscription) onRelationDeleted(c context.Context, e types.RawEvent) error {
	if m.onRelationDeletedHandler == nil {
		return nil
	}
	event := RelationDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onRelationDeletedHandler(c, event)
}

// OnMachineShared registers a handler for MachineSharedEvent
func (m *MachineEventSubscription) OnMachineShared(h func(c context.Context, h MachineSharedEvent) error) {
	m.onMachineSharedHandler = h
}

func (m *MachineEventSubscription) onMachineShared(c context.Context, e types.RawEvent) error {
	if m.onMachineSharedHandler == nil {
		return nil
	}
	event := MachineSharedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineSharedHandler(c, event)
}

// OnMachineUnshared registers a handler for MachineUnsharedEvent
func (m *MachineEventSubscription) OnMachineUnshared(h func(c context.Context, h MachineUnsharedEvent) error) {
	m.onMachineUnsharedHandler = h
}

func (m *MachineEventSubscription) onMachineUnshared(c context.Context, e types.RawEvent) error {
	if m.onMachineUnsharedHandler == nil {
		return nil
	}
	event := MachineUnsharedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineUnsharedHandler(c, event)
}

// OnMachineCreated registers a handler for MachineCreatedEvent
func (m *MachineEventSubscription) OnMachineCreated(h func(c context.Context, h MachineCreatedEvent) error) {
	m.onMachineCreatedHandler = h
}

func (m *MachineEventSubscription) onMachineCreated(c context.Context, e types.RawEvent) error {
	if m.onMachineCreatedHandler == nil {
		return nil
	}
	event := MachineCreatedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineCreatedHandler(c, event)
}

// OnMachineDeleted registers a handler for MachineDeletedEvent
func (m *MachineEventSubscription) OnMachineDeleted(h func(c context.Context, h MachineDeletedEvent) error) {
	m.onMachineDeletedHandler = h
}

func (m *MachineEventSubscription) onMachineDeleted(c context.Context, e types.RawEvent) error {
	if m.onMachineDeletedHandler == nil {
		return nil
	}
	event := MachineDeletedEvent{}
	if err := json.Unmarshal(e.RawMessage, &event); err != nil {
		return err
	}

	return m.onMachineDeletedHandler(c, event)
}
