package machineevents

import (
	"context"
	"testing"

	"bitbucket.org/innius/go-sns/types"

	"github.com/stretchr/testify/assert"
)

func TestSubscriptions(t *testing.T) {
	evt := CompanyCreatedEvent{}
	b, err := types.NewEventMessage(evt)
	assert.NoError(t, err)

	mySubscription := &MachineEventSubscription{}

	t.Run("without subscription", func(t *testing.T) {
		assert.NoError(t, mySubscription.EventHandler(context.Background(), *b))
	})

	t.Run("unsupported event", func(t *testing.T) {
		evt := struct{}{}
		b, err := types.NewEventMessage(evt)
		assert.NoError(t, err)
		assert.NoError(t, mySubscription.EventHandler(context.Background(), *b))
	})
}

func TestCompanyCreatedEvent(t *testing.T) {
	s := &MachineEventSubscription{}
	fired := false
	s.OnCompanyCreated(func(c context.Context, e CompanyCreatedEvent) error {
		fired = true
		return nil
	})
	b, err := types.NewEventMessage(CompanyCreatedEvent{})
	assert.NoError(t, err)
	assert.NoError(t, s.EventHandler(context.Background(), *b))
	assert.True(t, fired)
}

func TestCompanyDeletedEvent(t *testing.T) {
	s := &MachineEventSubscription{}
	fired := false
	s.OnCompanyDeleted(func(c context.Context, e CompanyDeletedEvent) error {
		fired = true
		return nil
	})
	b, err := types.NewEventMessage(CompanyDeletedEvent{})
	assert.NoError(t, err)
	assert.NoError(t, s.EventHandler(context.Background(), *b))
	assert.True(t, fired)
}

func TestPersonCreatedEvent(t *testing.T) {
	s := &MachineEventSubscription{}
	fired := false
	s.OnPersonCreated(func(c context.Context, e PersonCreatedEvent) error {
		fired = true
		return nil
	})
	b, err := types.NewEventMessage(PersonCreatedEvent{})
	assert.NoError(t, err)
	assert.NoError(t, s.EventHandler(context.Background(), *b))
	assert.True(t, fired)
}

func TestPersonDeletedEvent(t *testing.T) {
	s := &MachineEventSubscription{}
	fired := false
	s.OnPersonDeleted(func(c context.Context, e PersonDeletedEvent) error {
		fired = true
		return nil
	})
	b, err := types.NewEventMessage(PersonDeletedEvent{})
	assert.NoError(t, err)
	assert.NoError(t, s.EventHandler(context.Background(), *b))
	assert.True(t, fired)
}

func TestMachineCreatedEvent(t *testing.T) {
	s := &MachineEventSubscription{}
	fired := false
	s.OnMachineCreated(func(c context.Context, e MachineCreatedEvent) error {
		fired = true
		return nil
	})
	b, err := types.NewEventMessage(MachineCreatedEvent{})
	assert.NoError(t, err)
	assert.NoError(t, s.EventHandler(context.Background(), *b))
	assert.True(t, fired)
}

func TestMachineDeletedEvent(t *testing.T) {
	s := &MachineEventSubscription{}
	fired := false
	s.OnMachineDeleted(func(c context.Context, e MachineDeletedEvent) error {
		fired = true
		return nil
	})
	b, err := types.NewEventMessage(MachineDeletedEvent{})
	assert.NoError(t, err)
	assert.NoError(t, s.EventHandler(context.Background(), *b))
	assert.True(t, fired)
}

func TestMachineSharedEvent(t *testing.T) {
	s := &MachineEventSubscription{}
	fired := false
	s.OnMachineShared(func(c context.Context, e MachineSharedEvent) error {
		fired = true
		return nil
	})
	b, err := types.NewEventMessage(MachineSharedEvent{})
	assert.NoError(t, err)
	assert.NoError(t, s.EventHandler(context.Background(), *b))
	assert.True(t, fired)
}

func TestMachineUnsharedEvent(t *testing.T) {
	s := &MachineEventSubscription{}
	fired := false
	s.OnMachineUnshared(func(c context.Context, e MachineUnsharedEvent) error {
		fired = true
		return nil
	})
	b, err := types.NewEventMessage(MachineUnsharedEvent{})
	assert.NoError(t, err)
	assert.NoError(t, s.EventHandler(context.Background(), *b))
	assert.True(t, fired)
}

func TestAdminCreatedEvent(t *testing.T) {
	s := &MachineEventSubscription{}
	fired := false
	s.OnAdminCreated(func(c context.Context, e AdminCreatedEvent) error {
		fired = true
		return nil
	})
	b, err := types.NewEventMessage(AdminCreatedEvent{})
	assert.NoError(t, err)
	assert.NoError(t, s.EventHandler(context.Background(), *b))
	assert.True(t, fired)
}

func TestAdminDeletedEvent(t *testing.T) {
	s := &MachineEventSubscription{}
	fired := false
	s.OnAdminDeleted(func(c context.Context, e AdminDeletedEvent) error {
		fired = true
		return nil
	})
	b, err := types.NewEventMessage(AdminDeletedEvent{})
	assert.NoError(t, err)
	assert.NoError(t, s.EventHandler(context.Background(), *b))
	assert.True(t, fired)
}

func TestRelationVerifiedEvent(t *testing.T) {
	s := &MachineEventSubscription{}
	fired := false

	s.OnRelationVerified(func(c context.Context, e RelationVerifiedEvent) error {
		fired = true
		return nil
	})
	b, err := types.NewEventMessage(RelationVerifiedEvent{})
	assert.NoError(t, err)
	assert.NoError(t, s.EventHandler(context.Background(), *b))
	assert.True(t, fired)
}

func TestRelationDeletedEvent(t *testing.T) {
	s := &MachineEventSubscription{}
	fired := false

	s.OnRelationDeleted(func(c context.Context, e RelationDeletedEvent) error {
		fired = true
		return nil
	})
	b, err := types.NewEventMessage(RelationDeletedEvent{})
	assert.NoError(t, err)
	assert.NoError(t, s.EventHandler(context.Background(), *b))
	assert.True(t, fired)
}
