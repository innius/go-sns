package machineevents

import (
	"encoding/json"
)

// CompanyCreatedEvent is sent if a new company is created
type CompanyCreatedEvent struct {
	CompanyID string `json:"company_id"`
	Domain    string `json:"domain"`
	Name      string `json:"name"`
	Address   string `json:"address"`
	Zipcode   string `json:"zipcode"`
	City      string `json:"city"`
	Country   string `json:"country"`
	Status    string `json:"status"`
}

// CompanyDeletedEvent is sent if a company is deleted
type CompanyDeletedEvent struct {
	CompanyID string   `json:"company_id"`
	Domain    string   `json:"domain"`
	Name      string   `json:"name"`
	Machines  []string `json:"machines"`
}

// PersonDeletedEvent is sent if the specified person is deleted
type PersonDeletedEvent struct {
	CompanyID string `json:"company_id"`
	PersonID  string `json:"person_id"`
	Email     string `json:"email"`
	Name      string `json:"name"`
}

// PersonCreatedEvent is sent if the specified person is created
type PersonCreatedEvent struct {
	CompanyID string `json:"company_id"`
	PersonID  string `json:"person_id"`
	Email     string `json:"email"`
	Name      string `json:"name"`
}

// AdminCreatedEvent is sent if a new admin is created
type AdminCreatedEvent struct {
	CompanyID string `json:"company_id"`
	PersonID  string `json:"person_id"`
	Email     string `json:"email"`
	Name      string `json:"name"`
}

// AdminDeletedEvent is sent if an admin is deleted
type AdminDeletedEvent struct {
	CompanyID string `json:"company_id"`
	PersonID  string `json:"person_id"`
	Email     string `json:"email"`
	Name      string `json:"name"`
}
type RelationVerifiedEvent struct {
	CompanyID          string `json:"company_id"`
	CompanyName        string `json:"company_name"`
	RelatedCompanyID   string `json:"related_company_id"`
	RelatedCompanyName string `json:"related_company_name"`
}

type RelationDeletedEvent struct {
	CompanyID1 string `json:"company_id_1"`
	CompanyID2 string `json:"company_id_2"`
}

// MachineCreatedEvent is sent if a new machine is created
type MachineCreatedEvent struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// MachineDeletedEvent is sent if a machine is deleted
type MachineDeletedEvent struct {
	ID string `json:"id"`
}

type MachineSharedEvent struct {
	MachineID  string `json:"machine_id"`
	CompanyID  string `json:"company_id"`
	SharedWith string `json:"shared_with"`
}

type MachineUnsharedEvent struct {
	MachineID  string `json:"machine_id,omitempty"`
	CompanyID  string `json:"company_id,omitempty"`
	SharedWith string `json:"shared_with,omitempty"`
}

type MachineEventMessage struct {
	EventType  string `json:"event_type,omitempty"`
	RawMessage json.RawMessage
}
