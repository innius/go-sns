package kpis

// KpiCreatedEvent is sent when a new KPI is configured
type KpiCreatedEvent struct {
	MachineID string `json:"machinetrn"`
	KPI       string `json:"kpi"`
}

// KpiDeletedEvent is sent when a KPI is deleted
type KpiDeletedEvent struct {
	MachineID string `json:"machinetrn"`
	KPI       string `json:"kpi"`
}
