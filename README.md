# go-sns 

Thin wrapper around AWS SNS. Used for subscribing to events broadcasted by other services
or publishing them.

# usage 

```
import bitbucket.org/innius/go-sns/subscriber
import bitbucket.org/innius/go-sns/machine_events
```

1. Create a sns subscriber 

```
endpoint := subscriber.CreateEndpointString("/sns/bar")
snsSubscr := subscriber.NewSubscriber(snsClient, "my-topic", endpoint)

```

2. Create an event subscription 
```
mySubscription := &machineevents.MachineEventSubscription{}
```

3. Register the listener 

```
e.POST("/sns/bar", snsSubscr.Handler(mySubscription))
```

4. Subscribe the the appropriate events 
```
mySubscription.OnCompanyCreated(func(c context.Context, e machines.CompanyCreatedEvent) error {
    // your logic here .....
    return nil
})
```
