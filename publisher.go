package publisher

import (
	"context"
	"encoding/json"

	"bitbucket.org/innius/go-sns/types"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sns/snsiface"
	"github.com/pkg/errors"
)

func NewPublisher(client snsiface.SNSAPI, topicArn string) Publisher {
	return &SnsPublisher{topicArn, client}
}

type Publisher interface {
	// publish an event
	Publish(context.Context, interface{}) error
}

type SnsPublisher struct {
	topicArn string
	client   snsiface.SNSAPI
}

func (p *SnsPublisher) Publish(ctx context.Context, event interface{}) error {
	raw, err := types.NewEventMessage(event)
	if err != nil {
		return err
	}
	return p.PublishRawEvent(ctx, raw)
}

func (p *SnsPublisher) PublishRawEvent(ctx context.Context, r *types.RawEvent) error {
	bits, err := json.Marshal(r)
	if err != nil {
		return errors.Wrap(err, "Unable to marshal event")
	}
	if ctx == nil {
		return errors.New("Invalid context")
	}
	_, err = p.client.PublishWithContext(ctx, &sns.PublishInput{
		Message:  aws.String(string(bits)),
		TopicArn: aws.String(p.topicArn),
	})
	if err != nil {
		return errors.Wrap(err, "Failed to publish event")
	}
	return nil
}
