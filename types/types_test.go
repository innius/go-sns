package types_test

import (
	"bitbucket.org/innius/go-sns/types"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestGetType(t *testing.T) {
	Convey("Get event type from event", t, func() {
		evt := types.RawEvent{}
		So(types.EventType(evt), ShouldEqual, "RawEvent")
	})
}

func TestGetEventSource(t *testing.T) {
	Convey("Get event source from struct", t, func() {
		evt := types.RawEvent{}
		So(types.EventSource(evt), ShouldEqual, "types")
	})
	Convey("Get event source from pointer", t, func() {
		evt := &types.RawEvent{}
		So(types.EventSource(evt), ShouldEqual, "types")
	})
}
