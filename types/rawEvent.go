package types

import (
	"encoding/json"
	"reflect"
	"strings"

	"github.com/pkg/errors"
)

type RawEvent struct {
	EventType  string `json:"event_type,omitempty"`
	RawMessage json.RawMessage
}

func NewEventMessage(event interface{}) (*RawEvent, error) {
	payload, err := json.Marshal(event)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal event")
	}

	return &RawEvent{
		EventType:  EventType(event),
		RawMessage: payload,
	}, nil
}

func EventType(e interface{}) string {
	return reflect.TypeOf(e).Name()
}

// EventSource extracts the event source for event message e from the package path
func EventSource(e interface{}) string {
	tp := reflect.TypeOf(e)
	if tp.Kind() == reflect.Ptr {
		elem := reflect.ValueOf(e).Elem()
		tp = elem.Type()
	}
	path := tp.PkgPath()
	p := strings.Split(path, "/")
	return p[len(p)-1]
}
