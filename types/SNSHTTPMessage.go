package types

type SNSHTTPMessage struct {
	Message          string `json:"Message"`
	MessageID        string `json:"MessageID"`
	Type             string `json:"Type"`
	TopicArn         string `json:"TopicArn,omitempty"`
	Token            string `json:"Token,omitempty"`
	Timestamp        string `json:"Timestamp"`
	SubcribeURL      string `json:"SubscribeURL,omitempty"`
	Signature        string `json:"Signature"`
	SignatureVersion string `json:"SignatureVersion"`
}
