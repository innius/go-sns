package subscriber

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"bitbucket.org/innius/go-sns/types"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sns/snsiface"
	"github.com/labstack/echo"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const subscriptionConfirmation = "SubscriptionConfirmation"

//Convenience function for turning a path into an url using env variables.
func CreateEndpointString(path string) string {
	env := os.Getenv("ENVIRONMENT_NAME")
	// some services have EnvironmentName :-(
	if env == "" {
		env = os.Getenv("EnvironmentName")
	}
	serviceName := os.Getenv("SERVICE_NAME")
	return createEndpointString(env, serviceName, path)
}

func createEndpointString(environment, serviceName, path string) string {
	return fmt.Sprintf("%v/%v/%v", getExternalBaseUri(environment), serviceName, path)
}

// Get the (external) base name of a whole stack.
func getExternalBaseUri(stackname string) string {
	return fmt.Sprintf("https://%v.innius.com", getExternalName(stackname))
}

// Return the external name of the given stack.
// This will return a 'symbolic' name, such as 'live', 'dev', etc.
func getExternalName(stackname string) string {
	switch stackname {
	case "chair":
		return "test"
	case "oak":
		return "demo"
	case "steam":
		return "live"
	default:
		return stackname
	}
}

func NewSubscriber(client snsiface.SNSAPI, topic, endpoint string) Subscriber {
	return &subscriber{
		client:   client,
		topic:    topic,
		endpoint: endpoint,
	}
}

type Subscriber interface {
	Subscribe() error

	// Handler is a echo compatible Handler;
	// Deprecated
	Handler(EventHandler) echo.HandlerFunc

	// http Handler func
	HttpHandler(EventHandler) http.HandlerFunc
}

type subscriber struct {
	client   snsiface.SNSAPI
	topic    string
	endpoint string
}

type EventHandler interface {
	EventHandler(c context.Context, e types.RawEvent) error
}

// standard http middleware handler
func (s *subscriber) HttpHandler(h EventHandler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		m := &types.SNSHTTPMessage{}

		defer r.Body.Close()
		if err := json.NewDecoder(r.Body).Decode(m); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if m.Type == subscriptionConfirmation {
			if err := s.confirmSubscription(r.Context(), m.Token, m.TopicArn); err != nil {
				http.Error(w, "Failed to confirm http endpoint subscription", http.StatusExpectationFailed)
				return
			}
			w.WriteHeader(http.StatusOK)
			return
		}

		event := types.RawEvent{}
		err := json.Unmarshal([]byte(m.Message), &event)
		if err != nil {
			logrus.Errorf("%+v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		fmt.Printf("%+v", event)
		if err := h.EventHandler(r.Context(), event); err != nil {
			logrus.Errorf("could not handle event %+v; %+v", event, err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})
}

func (s *subscriber) Handler(h EventHandler) echo.HandlerFunc {
	return echo.WrapHandler(s.HttpHandler(h))

	//return func(c echo.Context) error {
	//	m := &types.SNSHTTPMessage{}
	//
	//	if err := json.NewDecoder(c.Request().Body).Decode(m); err != nil {
	//		return c.String(http.StatusBadRequest, err.Error())
	//	}
	//
	//	if m.Type == subscriptionConfirmation {
	//		if err := s.confirmSubscription(c.Request().Context(), m.Token, m.TopicArn); err != nil {
	//			return c.String(http.StatusExpectationFailed, "Failed to confirm http endpoint subscription")
	//		}
	//		return c.NoContent(http.StatusOK)
	//	}
	//	event := types.RawEvent{}
	//	err := json.Unmarshal([]byte(m.Message), &event)
	//	if err != nil {
	//		c.Logger().Errorf("%+v", err)
	//		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	//	}
	//	return h.EventHandler(c.Request().Context(), event)
	//}
}

//Keep attempting to subscribe until it succeeds. topicARN is the arn of the topic
//you want to subscribe to. endPoint is the callback endpoint, including the https part.
func (s *subscriber) Subscribe() error {
	for {
		if err := s.subscribe(); err != nil {
			logrus.Errorf("could not subscribe to sns topic; %+v", err)
		}

		if s.subscriptionConfirmed() {
			return nil
		}

		time.Sleep(60 * time.Second)
	}
}

func (s *subscriber) subscribe() error {
	_, err := s.client.Subscribe(&sns.SubscribeInput{
		Endpoint: aws.String(s.endpoint),
		TopicArn: aws.String(s.topic),
		Protocol: aws.String("https"),
	})

	return errors.Wrap(err, "could not subscribe to topic")
}

func (s *subscriber) confirmSubscription(c context.Context, token, topicArn string) error {
	_, err := s.client.ConfirmSubscriptionWithContext(c, &sns.ConfirmSubscriptionInput{
		Token:    aws.String(token),
		TopicArn: aws.String(topicArn),
	})
	return errors.Wrap(err, "could not confirm subscription")
}

func (s *subscriber) subscriptionConfirmed() bool {
	res, err := s.client.ListSubscriptionsByTopic(&sns.ListSubscriptionsByTopicInput{
		TopicArn: aws.String(s.topic),
	})

	if err != nil {
		logrus.Errorf("could not list subscriptions by topic; %+v", err)
		return false
	}
	confirmed := false
	for _, subscr := range res.Subscriptions {

		if aws.StringValue(subscr.Endpoint) == s.endpoint {
			confirmed = strings.HasPrefix(aws.StringValue(subscr.SubscriptionArn), "arn")
		}
	}
	return confirmed
}
