package subscriber

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/labstack/echo"

	"bitbucket.org/innius/go-sns/types"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/stretchr/testify/assert"
)

func TestTopicListener(t *testing.T) {
	listener := NewSubscriber(&SNSMock{}, "", "")
	timeout := time.After(time.Second)
	resultC := make(chan interface{}, 1)
	go func() {
		listener.Subscribe()
		resultC <- nil
	}()
	select {
	case <-resultC:
	case <-timeout:
		t.Fail()
	}
}

func TestConfirmSubscription(t *testing.T) {
	listener := subscriber{client: &SNSMock{}}

	err := listener.confirmSubscription(context.Background(), "foo", "bar")
	assert.NoError(t, err)
}

type mockSubscription struct {
	handler func(c context.Context, e types.RawEvent) error
}

func (m *mockSubscription) EventHandler(c context.Context, e types.RawEvent) error {
	return m.handler(c, e)
}

func TestSnsEventHandlerConfirm(t *testing.T) {
	snsMessage := types.SNSHTTPMessage{
		Type:     subscriptionConfirmation,
		TopicArn: "foo",
		Token:    "bar",
	}
	bits, err := json.Marshal(snsMessage)
	assert.NoError(t, err)

	listener := subscriber{client: &SNSMock{}}
	subscr := &mockSubscription{
		handler: func(c context.Context, r types.RawEvent) error {
			assert.Fail(t, "handler should not be invoked for confirm message")
			return nil
		},
	}
	h := listener.Handler(subscr)

	e := echo.New()
	w := httptest.NewRecorder()
	r := httptest.NewRequest(echo.POST, "http://foo.bar", bytes.NewReader(bits))
	r.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

	c := e.NewContext(r, w)

	assert.NoError(t, h(c))
	assert.Equal(t, http.StatusOK, w.Code)
}

func TestSnsEventHandlerMessage(t *testing.T) {
	evt := struct{}{}
	msg, err := types.NewEventMessage(evt)
	assert.NoError(t, err)

	b, err := json.Marshal(msg)
	assert.NoError(t, err)

	snsMessage := types.SNSHTTPMessage{
		Message: string(b),
	}

	bits, err := json.Marshal(snsMessage)
	assert.NoError(t, err)

	listener := subscriber{client: &SNSMock{}}

	fired := false
	subscr := &mockSubscription{
		handler: func(c context.Context, r types.RawEvent) error {
			fired = true
			return nil
		},
	}
	h := listener.Handler(subscr)

	e := echo.New()
	w := httptest.NewRecorder()
	r := httptest.NewRequest(echo.POST, "http://foo.bar", bytes.NewReader(bits))
	r.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

	c := e.NewContext(r, w)

	assert.NoError(t, h(c))
	assert.Equal(t, http.StatusOK, w.Code)
	assert.True(t, fired)

}

type SNSMock struct {
	*sns.SNS
}

func (m *SNSMock) Subscribe(input *sns.SubscribeInput) (*sns.SubscribeOutput, error) {
	return nil, nil
}

func (m *SNSMock) ConfirmSubscriptionWithContext(ctx aws.Context, input *sns.ConfirmSubscriptionInput, options ...request.Option) (*sns.ConfirmSubscriptionOutput, error) {
	return nil, nil
}

func (m *SNSMock) PublishWithContext(ctx aws.Context, input *sns.PublishInput, options ...request.Option) (*sns.PublishOutput, error) {
	return nil, nil
}

func (m *SNSMock) ListSubscriptionsByTopic(*sns.ListSubscriptionsByTopicInput) (*sns.ListSubscriptionsByTopicOutput, error) {
	return &sns.ListSubscriptionsByTopicOutput{
		Subscriptions: []*sns.Subscription{
			{SubscriptionArn: aws.String("arn:foo:bar")},
		},
	}, nil
}
